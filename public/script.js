async function logPackages() {
    const response = await fetch("packages.json");
    const packages = await response.json();
    packages.forEach(parsePackage);
}

function parsePackage(package) {
	let packageDomain = package.domain;
    let projectId = package.project_id;
    let packageName = package.name;
    let packageFile = "devcontainer-feature-" + packageName;
    let packageVersions = package.versions;
    
    document.getElementById("packages").innerHTML += `
        <div class="accordion-item">
        <h2 class="accordion-header">
        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#${packageName}" aria-expanded="false" aria-controls="${packageName}">
        ${packageName}
        </button>
        </h2>
        <div id="${packageName}" class="accordion-collapse collapse" data-bs-parent="#packages">
        <div id="${packageName}-versions" class="accordion-body">`;
    
    for (let i = packageVersions.length - 1; i > -1; i--) {
    	let packageUrl = `https://${packageDomain}/api/v4/projects/${projectId}/packages/generic/${packageName}/${packageVersions[i]}/${packageFile}.tgz`;
    	document.getElementById(`${packageName}-versions`).innerHTML += `
            <button type="button" class="btn-sm btn btn-primary m-1" onclick='clipboardPackageLink("${packageUrl}")'>
            ${packageVersions[i]}
            </button>`;
    }
    
    document.getElementById("packages").innerHTML += "</div></div></div>";
}

function clipboardPackageLink(stringText) {
    navigator.clipboard.writeText(stringText);
}
